'''
 * Copyright (c) 2014, 2015 Entertainment Intelligence Lab, Georgia Institute of Technology.
 * Originally developed by Mark Riedl.
 * Last edited by Mark Riedl 05/2015
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
'''

import sys, pygame, math, numpy, random, time, copy
from pygame.locals import *

from constants import *
from utils import *
from core import *
from mycreatepathnetwork import *
from mynavigatorhelpers import *


###############################
### AStarNavigator
###
### Creates a path node network and implements the A* algorithm to create a path to the given destination.

class AStarNavigator(NavMeshNavigator):
    def __init__(self):
        NavMeshNavigator.__init__(self)

    ### Create the path node network.
    ### self: the navigator object
    ### world: the world object
    def createPathNetwork(self, world):
        self.pathnodes, self.pathnetwork, self.navmesh = myCreatePathNetwork(world, self.agent)
        return None

    ### Finds the shortest path from the source to the destination using A*.
    ### self: the navigator object
    ### source: the place the agent is starting from (i.e., its current location)
    ### dest: the place the agent is told to go to
    def computePath(self, source, dest):
        self.setPath(None)
        ### Make sure the next and dist matrices exist
        if self.agent != None and self.world != None:
            self.source = source
            self.destination = dest
            ### Step 1: If the agent has a clear path from the source to dest, then go straight there.
            ###   Determine if there are no obstacles between source and destination (hint: cast rays against world.getLines(), check for clearance).
            ###   Tell the agent to move to dest
            ### Step 2: If there is an obstacle, create the path that will move around the obstacles.
            ###   Find the path nodes closest to source and destination.
            ###   Create the path by traversing the self.next matrix until the path node closest to the destination is reached
            ###   Store the path by calling self.setPath()
            ###   Tell the agent to move to the first node in the path (and pop the first node off the path)
            if clearShot(source, dest, self.world.getLines(), self.world.getPoints(), self.agent):
                self.agent.moveToTarget(dest)
            else:
                start = findClosestUnobstructed(source, self.pathnodes, self.world.getLinesWithoutBorders())
                end = findClosestUnobstructed(dest, self.pathnodes, self.world.getLinesWithoutBorders())
                if start != None and end != None:
                    # print len(self.pathnetwork)
                    newnetwork = unobstructedNetwork(self.pathnetwork, self.world.getGates())
                    # print len(newnetwork)
                    closedlist = []
                    path, closedlist = astar(start, end, newnetwork)
                    if path is not None and len(path) > 0:
                        path = shortcutPath(source, dest, path, self.world, self.agent)
                        self.setPath(path)
                        if self.path is not None and len(self.path) > 0:
                            first = self.path.pop(0)
                            self.agent.moveToTarget(first)
        return None

    ### Called when the agent gets to a node in the path.
    ### self: the navigator object
    def checkpoint(self):
        myCheckpoint(self)
        return None

    ### This function gets called by the agent to figure out if some shortcuts can be taken when traversing the path.
    ### This function should update the path and return True if the path was updated.
    def smooth(self):
        return mySmooth(self)

    def update(self, delta):
        myUpdate(self, delta)


def unobstructedNetwork(network, worldLines):
    newnetwork = []
    for l in network:
        hit = rayTraceWorld(l[0], l[1], worldLines)
        if hit == None:
            newnetwork.append(l)
    return newnetwork


def astar(init, goal, network):
    path = []
    open = []
    closed = []
    ### YOUR CODE GOES BELOW HERE ###
    open.append(init)
    lastNode = {}
    gValue = {init:0}
    temp = math.sqrt((init[0] - goal[0])**2 + (init[1] - goal[1])**2)
    fValue = {init:temp}
    while len(open) != 0:
        currentIdx = 0
        tempMinF = None
        for i in range(len(open)):
            temp = open[i]
            if tempMinF == None or fValue[temp] < tempMinF:
                tempMinF = fValue[temp]
                currentIdx = i
        current = open.pop(currentIdx)

        if current == goal:
            path.append(current)
            while current in lastNode:
                current = lastNode[current]
                path.append(current)
            path.reverse()
            break #Need further revise

        closed.append(current)
        for road in network:
            neighbor = None
            if road[0] == current:
                neighbor = road[1]
            elif road[1] == current:
                neighbor = road[0]
            else:
                continue

            if neighbor in closed:
                continue
            if neighbor not in open:
                open.append(neighbor)
            candgValue = gValue[current] + math.sqrt((current[0] - neighbor[0])**2 + (current[1] - neighbor[1])**2)
            if neighbor in gValue and candgValue >= gValue[neighbor]:
                continue

            lastNode[neighbor] = current
            gValue[neighbor] = candgValue
            fValue[neighbor] = candgValue + math.sqrt((neighbor[0] - goal[0])**2 + (neighbor[1] - goal[1])**2)

    ### YOUR CODE GOES ABOVE HERE ###
    return path, closed


def myUpdate(nav, delta):
    ### YOUR CODE GOES BELOW HERE ###
    for gate in nav.world.getLinesWithoutBorders():
        temp = rayTrace(nav.agent.moveOrigin, nav.agent.moveTarget, gate)
        if temp != None:
            nav.agent.stop()
            nav.computePath(nav.agent.position, nav.destination)
            break
    ### YOUR CODE GOES ABOVE HERE ###
    return None


def myCheckpoint(nav):
    ### YOUR CODE GOES BELOW HERE ###
    temppath = nav.path
    temppath = [nav.agent.moveTarget] + temppath
    for i in range(1, len(temppath)):
        p1 = temppath[i]
        p2 = temppath[i - 1]
        for gate in nav.world.getLinesWithoutBorders():
            temp = rayTrace(p1, p2, gate)
            if temp != None:
                nav.agent.stop()
                nav.computePath(nav.agent.position, nav.destination)
                return None
    ### YOUR CODE GOES ABOVE HERE ###
    return None


### Returns true if the agent can get from p1 to p2 directly without running into an obstacle.
### p1: the current location of the agent
### p2: the destination of the agent
### worldLines: all the lines in the world
### agent: the Agent object
def clearShot(p1, p2, worldLines, worldPoints, agent):
    ### YOUR CODE GOES BELOW HERE ###
    vec = numpy.array([p2[0] - p1[0], p2[1] - p1[1]])
    vecdis = numpy.linalg.norm(vec)
    vec = vec * agent.maxradius / vecdis
    vec[0] = (math.ceil(vec[0]) if vec[0] > 0 else math.floor(vec[0]))
    vec[1] = (math.ceil(vec[1]) if vec[1] > 0 else math.floor(vec[1]))

    nor = numpy.array([p1[1] - p2[1], p2[0] - p1[0]])
    nordis = numpy.linalg.norm(nor)
    nor = nor * agent.maxradius / nordis
    nor[0] = (math.ceil(nor[0]) if nor[0] > 0 else math.floor(nor[0]))
    nor[1] = (math.ceil(nor[1]) if nor[1] > 0 else math.floor(nor[1]))

    recpoint1 = (p1[0] - vec[0] - nor[0], p1[1] - vec[1] - nor[1])
    recpoint2 = (p1[0] - vec[0] + nor[0], p1[1] - vec[1] + nor[1])
    recpoint3 = (p2[0] + vec[0] - nor[0], p2[1] + vec[1] - nor[1])
    recpoint4 = (p2[0] + vec[0] + nor[0], p2[1] + vec[1] + nor[1])

    for line in worldLines:
        if rayTrace(recpoint1, recpoint2, line) != None:
            return False
        if rayTrace(recpoint1, recpoint3, line) != None:
            return False
        if rayTrace(recpoint2, recpoint4, line) != None:
            return False
        if rayTrace(recpoint3, recpoint4, line) != None:
            return False

    ### YOUR CODE GOES ABOVE HERE ###
    return True