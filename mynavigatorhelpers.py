'''
 * Copyright (c) 2014, 2015 Entertainment Intelligence Lab, Georgia Institute of Technology.
 * Originally developed by Mark Riedl.
 * Last edited by Mark Riedl 05/2015
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
'''

import sys, pygame, math, numpy, random, time, copy
from pygame.locals import * 

from constants import *
from utils import *
from core import *

### This function optimizes the given path and returns a new path
### source: the current position of the agent
### dest: the desired destination of the agent
### path: the path previously computed by the A* algorithm
### world: pointer to the world
def shortcutPath(source, dest, path, world, agent):
	### YOUR CODE GOES BELOW HERE ###
    start = 0
    path = [source] + path + [dest]
    newpath = []
    for i in range(len(path)):
        if i < start:
            continue
        newpath.append(path[i])
        for j in range(i + 1, len(path)):
            p1 = path[i]
            p2 = path[j]
            vec = numpy.array([p2[0] - p1[0], p2[1] - p1[1]])
            vecdis = numpy.linalg.norm(vec)
            vec = vec * agent.maxradius / vecdis
            vec[0] = (math.ceil(vec[0]) if vec[0] > 0 else math.floor(vec[0]))
            vec[1] = (math.ceil(vec[1]) if vec[1] > 0 else math.floor(vec[1]))

            nor = numpy.array([p1[1] - p2[1], p2[0] - p1[0]])
            nordis = numpy.linalg.norm(nor)
            nor = nor * agent.maxradius / nordis
            nor[0] = (math.ceil(nor[0]) if nor[0] > 0 else math.floor(nor[0]))
            nor[1] = (math.ceil(nor[1]) if nor[1] > 0 else math.floor(nor[1]))

            recpoint1 = (p1[0] - vec[0] - nor[0], p1[1] - vec[1] - nor[1])
            recpoint2 = (p1[0] - vec[0] + nor[0], p1[1] - vec[1] + nor[1])
            recpoint3 = (p2[0] + vec[0] - nor[0], p2[1] + vec[1] - nor[1])
            recpoint4 = (p2[0] + vec[0] + nor[0], p2[1] + vec[1] + nor[1])

            flag = False
            for line in world.getLines():
                if rayTrace(recpoint1, recpoint2, line) != None:
                    flag = True
                    break
                if rayTrace(recpoint1, recpoint3, line) != None:
                    flag = True
                    break
                if rayTrace(recpoint2, recpoint4, line) != None:
                    flag = True
                    break
                if rayTrace(recpoint3, recpoint4, line) != None:
                    flag = True
                    break
            if not flag:
                start = j

    newpath.pop(0)
    path = newpath
    ### YOUR CODE GOES BELOW HERE ###
    return path


### This function changes the move target of the agent if there is an opportunity to walk a shorter path.
### This function should call nav.agent.moveToTarget() if an opportunity exists and may also need to modify nav.path.
### nav: the navigator object
### This function returns True if the moveTarget and/or path is modified and False otherwise
def mySmooth(nav):
	### YOUR CODE GOES BELOW HERE ###
    #print 'mySmooth run'
    path = nav.path
    if path != None and len(path) != 0:
        p2 = path[0]
        p1 = nav.agent.position

        vec = numpy.array([p2[0] - p1[0], p2[1] - p1[1]])
        vecdis = numpy.linalg.norm(vec)
        vec = vec * nav.agent.maxradius / vecdis
        vec[0] = (math.ceil(vec[0]) if vec[0] > 0 else math.floor(vec[0]))
        vec[1] = (math.ceil(vec[1]) if vec[1] > 0 else math.floor(vec[1]))

        nor = numpy.array([p1[1] - p2[1], p2[0] - p1[0]])
        nordis = numpy.linalg.norm(nor)
        nor = nor * nav.agent.maxradius / nordis
        nor[0] = (math.ceil(nor[0]) if nor[0] > 0 else math.floor(nor[0]))
        nor[1] = (math.ceil(nor[1]) if nor[1] > 0 else math.floor(nor[1]))

        recpoint1 = (p1[0] - vec[0] - nor[0], p1[1] - vec[1] - nor[1])
        recpoint2 = (p1[0] - vec[0] + nor[0], p1[1] - vec[1] + nor[1])
        recpoint3 = (p2[0] + vec[0] - nor[0], p2[1] + vec[1] - nor[1])
        recpoint4 = (p2[0] + vec[0] + nor[0], p2[1] + vec[1] + nor[1])

        for line in nav.world.getLines():
            if rayTrace(recpoint1, recpoint2, line) != None:
                return False
            if rayTrace(recpoint1, recpoint3, line) != None:
                return False
            if rayTrace(recpoint2, recpoint4, line) != None:
                return False
            if rayTrace(recpoint3, recpoint4, line) != None:
                return False

        nextNode = path.pop(0)
        nav.setPath(path)
        nav.agent.moveToTarget(nextNode)
        return True
    ### YOUR CODE GOES ABOVE HERE ###
    return False

### This function changes the move target of the agent if there is an opportunity to walk a shorter path.
### This function should call nav.agent.moveToTarget() if an opportunity exists and may also need to modify nav.path.
### nav: the navigator object
### This function returns True if the moveTarget and/or path is modified and False otherwise
def mySmooth(nav):
	### YOUR CODE GOES BELOW HERE ###

	### YOUR CODE GOES ABOVE HERE ###
	return False


