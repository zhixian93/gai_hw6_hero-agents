'''
 * Copyright (c) 2014, 2015 Entertainment Intelligence Lab, Georgia Institute of Technology.
 * Originally developed by Mark Riedl.
 * Last edited by Mark Riedl 05/2015
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
'''

import sys, pygame, math, numpy, random, time, copy
from pygame.locals import *

from constants import *
from utils import *
from core import *
from moba2 import *
from btnode import *

###########################
### SET UP BEHAVIOR TREE


def treeSpec(agent):
    myid = str(agent.getTeam())
    spec = None
    ### YOUR CODE GOES BELOW HERE ###
    baseBT = [(LevelDaemon, "judge2"), [(Sequence, 11),(MyRetreat, 1, "transaction"), (MyMoveToTarget, (600,1100), 0.6, 100), (MyChaseBase, 0.6, 102),(MyKillHero, 0.6, 303), (MyKillBase, 0.6, 103), (MyMoveToTarget, (50, 1100), 0.6, 104)
              , (MyRunAway, (95, 581), 1, 98), (MyMidRetreat, 0.8, 99), (MyMoveToTarget, (870, 650), 0.6, 200), (MyChaseBase, 0.6, 202),(MyKillHero, 0.6, 203), (MyKillBase, 0.6, 203), (MyRunAway, (1016,565), 1.1, 204)
              , (MyMidRetreat, 0.6, 99)]]
    baseBT1 = [(AntiLevelDaemon, "judge"), [(Sequence, 301),  (MyHunt, 0.5, 302), (MyKillMinion, 0.5, 303),(MyKillHero, 0.6, 304),[(AvoidBaseDaemon, 305), (MyRunAway, (111,449),1.1, 306)], (MyRunAway,(111,449), 0.6, 307)]]

    spec = [(Selector, 1), [(Sequence, 31), (MyRunAway, (183, 483), 0.5, 1.8), (MyRetreat, 0.5, 2)]]
    spec.append(baseBT1)
    spec.append(baseBT)
    ### YOUR CODE GOES ABOVE HERE ###
    return spec

def myBuildTree(agent):
    myid = str(agent.getTeam())
    root = None
    ### YOUR CODE GOES BELOW HERE ###

    ### YOUR CODE GOES ABOVE HERE ###
    return root


### Helper function for making BTNodes (and sub-classes of BTNodes).
### type: class type (BTNode or a sub-class)
### agent: reference to the agent to be controlled
### This function takes any number of additional arguments that will be passed to the BTNode and parsed using BTNode.parseArgs()
def makeNode(type, agent, *args):
    node = type(agent, args)
    return node


###############################
### BEHAVIOR CLASSES:


##################
### Taunt
###
### Print disparaging comment, addressed to a given NPC
### Parameters:
###   0: reference to an NPC
###   1: node ID string (optional)

class Taunt(BTNode):
    ### target: the enemy agent to taunt

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        # First argument is the target
        if len(args) > 0:
            self.target = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.id = args[1]

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.target is not None:
            print "Hey", self.target, "I don't like you!"
        return ret


##################
### MoveToTarget
###
### Move the agent to a given (x, y)
### Parameters:
###   0: a point (x, y)
###   1: node ID string (optional)

class MoveToTarget(BTNode):
    ### target: a point (x, y)

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        # First argument is the target
        if len(args) > 0:
            self.target = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.agent.navigateTo(self.target)

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.target == None:
            # failed executability conditions
            print "exec", self.id, "false"
            return False
        elif distance(self.agent.getLocation(), self.target) < self.agent.getRadius():
            # Execution succeeds
            print "exec", self.id, "true"
            return True
        else:
            # executing
            return None
        return ret


##################
### Retreat
###
### Move the agent back to the base to be healed
### Parameters:
###   0: percentage of hitpoints that must have been lost to retreat
###   1: node ID string (optional)


class Retreat(BTNode):
    ### percentage: Percentage of hitpoints that must have been lost

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.percentage = 0.5
        # First argument is the factor
        if len(args) > 0:
            self.percentage = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.agent.navigateTo(self.agent.world.getBaseForTeam(self.agent.getTeam()).getLocation())

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() > self.agent.getMaxHitpoints() * self.percentage:
            # fail executability conditions
            print "exec", self.id, "false"
            return False
        elif self.agent.getHitpoints() == self.agent.getMaxHitpoints():
            # Exection succeeds
            print "exec", self.id, "true"
            return True
        else:
            # executing
            return None
        return ret


##################
### ChaseMinion
###
### Find the closest minion and move to intercept it.
### Parameters:
###   0: node ID string (optional)


class ChaseMinion(BTNode):
    ### target: the minion to chase
    ### timer: how often to replan

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        self.timer = 50
        # First argument is the node ID
        if len(args) > 0:
            self.id = args[0]

    def enter(self):
        BTNode.enter(self)
        self.timer = 50
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        if len(enemies) > 0:
            best = None
            dist = 0
            for e in enemies:
                if isinstance(e, Minion):
                    d = distance(self.agent.getLocation(), e.getLocation())
                    if best == None or d < dist:
                        best = e
                        dist = d
            self.target = best
        if self.target is not None:
            navTarget = self.chooseNavigationTarget()
            if navTarget is not None:
                self.agent.navigateTo(navTarget)

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.target == None or self.target.isAlive() == False:
            # failed execution conditions
            print "exec", self.id, "false"
            return False
        elif self.target is not None and distance(self.agent.getLocation(), self.target.getLocation()) < BIGBULLETRANGE:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            # executing
            self.timer = self.timer - 1
            if self.timer <= 0:
                self.timer = 50
                navTarget = self.chooseNavigationTarget()
                if navTarget is not None:
                    self.agent.navigateTo(navTarget)
            return None
        return ret

    def chooseNavigationTarget(self):
        if self.target is not None:
            return self.target.getLocation()
        else:
            return None


##################
### KillMinion
###
### Kill the closest minion. Assumes it is already in range.
### Parameters:
###   0: node ID string (optional)


class KillMinion(BTNode):
    ### target: the minion to shoot

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        # First argument is the node ID
        if len(args) > 0:
            self.id = args[0]

    def enter(self):
        BTNode.enter(self)
        self.agent.stopMoving()
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        if len(enemies) > 0:
            best = None
            dist = 0
            for e in enemies:
                if isinstance(e, Minion):
                    d = distance(self.agent.getLocation(), e.getLocation())
                    if best == None or d < dist:
                        best = e
                        dist = d
            self.target = best

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.target == None or distance(self.agent.getLocation(), self.target.getLocation()) > BIGBULLETRANGE:
            # failed executability conditions
            print "exec", self.id, "false"
            return False
        elif self.target.isAlive() == False:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            # executing
            self.shootAtTarget()
            return None
        return ret

    def shootAtTarget(self):
        if self.agent is not None and self.target is not None:
            self.agent.turnToFace(self.target.getLocation())
            self.agent.shoot()


##################
### ChaseHero
###
### Move to intercept the enemy Hero.
### Parameters:
###   0: node ID string (optional)

class ChaseHero(BTNode):
    ### target: the hero to chase
    ### timer: how often to replan

    def ParseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        self.timer = 50
        # First argument is the node ID
        if len(args) > 0:
            self.id = args[0]

    def enter(self):
        BTNode.enter(self)
        self.timer = 50
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        for e in enemies:
            if isinstance(e, Hero):
                self.target = e
                navTarget = self.chooseNavigationTarget()
                if navTarget is not None:
                    self.agent.navigateTo(navTarget)
                return None

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.target == None or self.target.isAlive() == False:
            # fails executability conditions
            print "exec", self.id, "false"
            return False
        elif distance(self.agent.getLocation(), self.target.getLocation()) < BIGBULLETRANGE:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            # executing
            self.timer = self.timer - 1
            if self.timer <= 0:
                navTarget = self.chooseNavigationTarget()
                if navTarget is not None:
                    self.agent.navigateTo(navTarget)
            return None
        return ret

    def chooseNavigationTarget(self):
        if self.target is not None:
            return self.target.getLocation()
        else:
            return None


##################
### KillHero
###
### Kill the enemy hero. Assumes it is already in range.
### Parameters:
###   0: node ID string (optional)


class KillHero(BTNode):
    ### target: the minion to shoot

    def ParseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        # First argument is the node ID
        if len(args) > 0:
            self.id = args[0]

    def enter(self):
        BTNode.enter(self)
        self.agent.stopMoving()
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        for e in enemies:
            if isinstance(e, Hero):
                self.target = e
                return None

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.target == None or distance(self.agent.getLocation(), self.target.getLocation()) > BIGBULLETRANGE:
            # failed executability conditions
            if self.target == None:
                print "foo none"
            else:
                print "foo dist", distance(self.agent.getLocation(), self.target.getLocation())
            print "exec", self.id, "false"
            return False
        elif self.target.isAlive() == False:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            # executing
            self.shootAtTarget()
            return None
        return ret

    def shootAtTarget(self):
        if self.agent is not None and self.target is not None:
            self.agent.turnToFace(self.target.getLocation())
            self.agent.shoot()


##################
### HitpointDaemon
###
### Only execute children if hitpoints are above a certain threshold.
### Parameters:
###   0: percentage of hitpoints that must have been lost to fail the daemon check
###   1: node ID string (optional)


class HitpointDaemon(BTNode):
    ### percentage: percentage of hitpoints that must have been lost to fail the daemon check

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.percentage = 0.5
        # First argument is the factor
        if len(args) > 0:
            self.percentage = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.id = args[1]

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() < self.agent.getMaxHitpoints() * self.percentage:
            # Check failed
            print "exec", self.id, "fail"
            return False
        else:
            # Check didn't fail, return child's status
            return self.getChild(0).execute(delta)
        return ret


##################
### BuffDaemon
###
### Only execute children if agent's level is significantly above enemy hero's level.
### Parameters:
###   0: Number of levels above enemy level necessary to not fail the check
###   1: node ID string (optional)

class BuffDaemon(BTNode):
    ### advantage: Number of levels above enemy level necessary to not fail the check

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.advantage = 0
        # First argument is the advantage
        if len(args) > 0:
            self.advantage = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.id = args[1]

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        hero = None
        # Get a reference to the enemy hero
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        for e in enemies:
            if isinstance(e, Hero):
                hero = e
                break
        if hero == None or self.agent.level <= hero.level + self.advantage:
            # fail check
            print "exec", self.id, "fail"
            return False
        else:
            # Check didn't fail, return child's status
            return self.getChild(0).execute(delta)
        return ret


#################################
### MY CUSTOM BEHAVIOR CLASSES

class LevelDaemon(BTNode):

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        if len(args) > 0:
            self.id = args[0]

    def execute(self, delta = 0):
        if self.agent.world.getScore(self.agent.getTeam()) >= 100:
            return self.getChild(0).execute(delta)
        else:
            return False

class AntiLevelDaemon(BTNode):

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        if len(args) > 0:
            self.id = args[0]

    def execute(self, delta = 0):
        if self.agent.world.getScore(self.agent.getTeam()) < 100:
            return self.getChild(0).execute(delta)
        else:
            return False


class AvoidBaseDaemon(BTNode):
    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        if len(args) > 0:
            self.id = args[0]

    def enter(self):
        BTNode.enter(self)
        self.base = self.agent.world.getEnemyBases(self.agent.getTeam())[0]

    def execute(self, delta = 0):
        ret = BTNode.execute(self, delta)
        if distance(self.base.getLocation(), self.agent.getLocation()) <= 1.5 * BIGBULLETRANGE:
            return self.getChild(0).execute(delta)
        else:
            return True



class MyChaseBase(BTNode):
    ### take retreat percentage & id
    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        self.percentage = None
        self.hero = None
        self.timer = 50
        # First argument is the node ID
        if len(args) > 0:
            self.percentage = args[0]
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.timer = 50
        enemyBases = self.agent.world.getEnemyBases(self.agent.getTeam())
        if len(enemyBases) > 0:
            for e in enemyBases:
                if isinstance(e, Base):
                    self.target = e
                    break
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        if len(enemies) > 0:
            for e in enemies:
                if isinstance(e, Hero):
                    self.hero = e
                    break
        if self.target is not None:
            navTarget = self.chooseNavigationTarget()
            if navTarget is not None:
                self.agent.navigateTo(navTarget)


    def execute(self, delta = 0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() <= self.percentage * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return False
        if self.hero != None and distance(self.hero.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy hero discover!"
            return True
        if self.target == None or self.target.isAlive() == False:
            # failed execution conditions
            print "exec", self.id, "false"
            return False
        elif self.target is not None and distance(self.agent.getLocation(), self.target.getLocation()) < BIGBULLETRANGE:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
                if distance(e.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
                    self.agent.turnToFace(e.getLocation())
                    self.agent.shoot()
                    break
            # executing
            self.timer = self.timer - 1
            if self.timer <= 0:
                self.timer = 50
                navTarget = self.chooseNavigationTarget()
                if navTarget is not None:
                    self.agent.navigateTo(navTarget)
            lines = self.agent.world.getLines()
            faster = True
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    faster = False
                    break
            if faster:
                movetarget = self.agent.moveTarget
                selfposition = self.agent.getLocation()
                if movetarget == None or selfposition == None:
                    return None
                ori = math.atan((selfposition[1] - movetarget[1]) / (movetarget[0] - selfposition[0])) / 3.14 * 180
                if movetarget[0] < selfposition[0]:
                    ori = ori - 180
                if ori < 0:
                    ori = ori + 360
                self.agent.dodge(ori)
            return None
        return ret

    def chooseNavigationTarget(self):
        if self.target is not None:
            return self.target.getLocation()
        else:
            return None




class MyKillBase(BTNode):
    ### take hitpoint percentage & id

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        self.percentage = None
        # First argument is the node ID
        if len(args) > 0:
            self.percentage = args[0]
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.agent.stopMoving()
        enemies = self.agent.world.getEnemyBases(self.agent.getTeam())
        if len(enemies) > 0:
            for e in enemies:
                if isinstance(e, Base):
                    self.target = e
                    break
        npcs = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        for e in npcs:
            if isinstance(e, Hero):
                self.hero = e
                break
        return None

    def execute(self, delta = 0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() <= self.percentage * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return False
        if self.hero != None and distance(self.hero.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy hero discover!"
            return True
        if self.target == None or distance(self.agent.getLocation(), self.target.getLocation()) > BIGBULLETRANGE:
            # failed executability conditions
            if self.target == None:
                print "foo none"
            else:
                print "foo dist", distance(self.agent.getLocation(), self.target.getLocation())
            print "exec", self.id, "false"
            return False
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        for e in enemies:
            if isinstance(e, Minion) and distance(e.getLocation(), self.agent.getLocation()) <= SMALLBULLETRANGE:
                self.agent.turnToFace(e.getLocation())
                self.agent.shoot()
                return None
        if self.target.isAlive() == False:
            # succeeded
            print "exec", self.id, "true"
            return True
        elif self.agent.getHitpoints() < 0.5 * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return True
        else:
            #executing
            # executing
            self.shootAtTarget()
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
            return None
        return ret

    def shootAtTarget(self):
        if self.agent is not None and self.target is not None:
            self.agent.turnToFace(self.target.getLocation())
            self.agent.shoot()



class MyHunt(BTNode):
    ### take retreat percentage & id

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.percentage = None
        self.target = None
        self.hero = None
        self.timer = 50
        # First argument is the node ID
        if len(args) > 0:
            self.percentage = args[0]
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.timer = 50
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        if len(enemies) > 0:
            best = None
            dist = 0
            for e in enemies:
                d = distance(self.agent.getLocation(), e.getLocation())
                if isinstance(e, Hero):
                    self.hero = e
                    if self.agent.getHitpoints() <= e.getHitpoints()  or self.agent.getHitpoints() < self.agent.getMaxHitpoints() * 0.8 or not self.agent.canAreaEffect():
                        continue
                if best == None or d < dist:
                    best = e
                    dist = d
            self.target = best
        bases = self.agent.world.getEnemyBases(self.agent.getTeam())
        self.base = bases[0]

        if self.target is not None:
            navTarget = self.chooseNavigationTarget()
            if navTarget is not None:
                self.agent.navigateTo(navTarget)

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints <= self.percentage * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return False
        if self.hero != None and distance(self.hero.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy hero discover!"
            return True
        if self.base != None and distance(self.base.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy base discover!"
            return True
        if self.target == None or self.target.isAlive() == False:
            # failed execution conditions
            print "exec", self.id, "false"
            return False
        elif self.target is not None and distance(self.agent.getLocation(), self.target.getLocation()) < SMALLBULLETRANGE:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
                if distance(e.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
                    self.agent.turnToFace(e.getLocation())
                    self.agent.shoot()
                    break
            # executing
            self.timer = self.timer - 1
            if self.timer <= 0:
                self.timer = 50
                navTarget = self.chooseNavigationTarget()
                if navTarget is not None:
                    self.agent.navigateTo(navTarget)
            lines = self.agent.world.getLines()
            faster = True
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    faster = False
                    break
            if faster:
                movetarget = self.agent.moveTarget
                selfposition = self.agent.getLocation()
                if movetarget == None or selfposition == None:
                    return None
                ori = math.atan((selfposition[1] - movetarget[1]) / (movetarget[0] - selfposition[0])) / 3.14 * 180
                if movetarget[0] < selfposition[0]:
                    ori = ori - 180
                if ori < 0:
                    ori = ori + 360
                self.agent.dodge(ori)
            return None
        return ret

    def chooseNavigationTarget(self):
        if self.target is not None:
            return self.target.getLocation()
        else:
            return None



class MyKillMinion(BTNode):
    ### take hitpoint percentage & id

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        self.percentage = None
        # First argument is the node ID
        if len(args) > 0:
            self.percentage = args[0]
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        if len(enemies) > 0:
            best = None
            dist = 0
            for e in enemies:
                if isinstance(e, Minion):
                    d = distance(self.agent.getLocation(), e.getLocation())
                    if best == None or d < dist:
                        best = e
                        dist = d
                if isinstance(e, Hero):
                    self.hero = e
            self.target = best
        bases = self.agent.world.getEnemyBases(self.agent.getTeam())
        self.base = bases[0]

    def execute(self, delta = 0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() <= self.percentage * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return False
        if self.hero != None and distance(self.hero.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy hero discover!"
            return True
        if self.base != None and distance(self.base.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy base discover!"
            return True
        if self.target == None or distance(self.agent.getLocation(), self.target.getLocation()) > BIGBULLETRANGE:
            # failed executability conditions
            if self.target == None:
                print "foo none"
            else:
                print "foo dist", distance(self.agent.getLocation(), self.target.getLocation())
            print "exec", self.id, "false"
            return False
        elif self.target.isAlive() == False:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            #executing
            self.shootAtTarget()
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
            enemyposition = self.target.getLocation()
            myposition = self.agent.getLocation()
            lines = self.agent.world.getLines()
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    return None
            arg = 0
            if self.agent.canAreaEffect and self.target.getHitpoints() <= AREAEFFECTDAMAGE:
                self.agent.navigateTo(self.target.getLocation())
                arg = math.atan((myposition[1] - enemyposition[1]) / (enemyposition[0] - myposition[0]))
                arg = arg / 3.1415926 * 180
                if enemyposition[0] < myposition[0]:
                    arg -= 180
                if arg < 0:
                    arg += 360
                if self.agent.canDodge:
                    self.agent.dodge(arg)
                if distance(self.target.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + self.target.getRadius():
                    self.agent.areaEffect()
            return None
        return ret

    def shootAtTarget(self):
        if self.agent is not None and self.target is not None:
            self.agent.turnToFace(self.target.getLocation())
            self.agent.shoot()


class MyChaseHero(BTNode):
    ### take hitpoint percentage & id

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.percentage = None
        self.target = None
        self.timer = 50
        # First argument is the node ID
        if len(args) > 0:
            self.percentage = args[0]
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.timer = 50
        bases = self.agent.world.getEnemyBases(self.agent.getTeam())
        self.base = bases[0]
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        for e in enemies:
            if isinstance(e, Hero):
                self.target = e
                navTarget = self.chooseNavigationTarget()
                if navTarget is not None:
                    self.agent.navigateTo(navTarget)

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() <= self.percentage * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return False
        if self.base != None and distance(self.base.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy base discover!"
            return False
        if self.target == None or self.target.isAlive() == False:
            # fails executability conditions
            print "exec", self.id, "false"
            return False
        elif distance(self.agent.getLocation(), self.target.getLocation()) < BIGBULLETRANGE:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
                if distance(e.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
                    self.agent.turnToFace(e.getLocation())
                    self.agent.shoot()
                    break
            # executing
            self.timer = self.timer - 1
            if self.timer <= 0:
                navTarget = self.chooseNavigationTarget()
                if navTarget is not None:
                    self.agent.navigateTo(navTarget)

            lines = self.agent.world.getLines()
            faster = True
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    faster = False
                    break
            if faster:
                movetarget = self.agent.moveTarget
                selfposition = self.agent.getLocation()
                if movetarget == None or selfposition == None:
                    return None
                ori = math.atan((selfposition[1] - movetarget[1]) / (movetarget[0] - selfposition[0])) / 3.14 * 180
                if movetarget[0] < selfposition[0]:
                    ori = ori - 180
                if ori < 0:
                    ori = ori + 360
                self.agent.dodge(ori)
            return None
        return ret

    def chooseNavigationTarget(self):
        if self.target is not None:
            return self.target.getLocation()
        else:
            return None



class MyKillHero(BTNode):
    ### take hitpoint percentage & id

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        self.percentage = None
        # First argument is the node ID
        if len(args) > 0:
            self.percentage = args[0]
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
        if len(enemies) > 0:
            for e in enemies:
                if isinstance(e, Hero):
                    self.target = e
                    break
        bases = self.agent.world.getEnemyBases(self.agent.getTeam())
        self.base = bases[0]

    def execute(self, delta = 0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() <= self.percentage * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return False
        if self.base != None and distance(self.base.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
            print "enemy base discover!"
            return True
        if self.target == None or distance(self.agent.getLocation(), self.target.getLocation()) > BIGBULLETRANGE:
            # failed executability conditions
            if self.target == None:
                print "foo none"
            else:
                print "foo dist", distance(self.agent.getLocation(), self.target.getLocation())
            print "exec", self.id, "false"
            return True
        elif self.target.isAlive() == False:
            # succeeded
            print "exec", self.id, "true"
            return True
        else:
            #executing
            self.shootAtTarget()
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
            enemyposition = self.target.getLocation()
            myposition = self.agent.getLocation()
            lines = self.agent.world.getLines()
            judge = False
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    judge = True
            arg = 0
            if self.agent.canAreaEffect() and (self.target.getHitpoints() <= AREAEFFECTDAMAGE or self.agent.getHitpoints() > self.target.getHitpoints() + BIGBULLETDAMAGE):
                self.agent.navigateTo(self.target.getLocation())
                if judge:
                    return None
                arg = math.atan((myposition[1] - enemyposition[1])/(enemyposition[0] - myposition[0]))
                arg = arg / 3.1415926 * 180
                if enemyposition[0] < myposition[0]:
                    arg -= 180
                if arg < 0:
                    arg += 360
                if self.agent.canDodge:
                    self.agent.dodge(arg)
                if distance(self.target.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + self.target.getRadius():
                    self.agent.areaEffect()
                    return True
            else:
                self.agent.navigateTo(self.agent.world.getBaseForTeam(self.agent.getTeam()).getLocation())
                if judge:
                    return None
                ori = self.agent.getOrientation()
                self.agent.dodge(ori)
                return None
            return None
        return ret

    def shootAtTarget(self):
        if self.agent is not None and self.target is not None:
            self.agent.turnToFace(self.target.getLocation())
            self.agent.shoot()



class MyRetreat(BTNode):
    ### percentage: Percentage of hitpoints that must have been lost

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.percentage = 0.5
        # First argument is the factor
        if len(args) > 0:
            self.percentage = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.agent.navigateTo(self.agent.world.getBaseForTeam(self.agent.getTeam()).getLocation())

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() > self.agent.getMaxHitpoints() * self.percentage:
            # fail executability conditions
            print "exec", self.id, "false"
            return False
        elif self.agent.getHitpoints() == self.agent.getMaxHitpoints():
            # Exection succeeds
            print "exec", self.id, "true"
            return True
        else:
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
                if distance(e.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
                    self.agent.turnToFace(e.getLocation())
                    self.agent.shoot()
                    break

            lines = self.agent.world.getLines()
            faster = True
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    faster = False
                    break
            if faster:
                movetarget = self.agent.moveTarget
                selfposition = self.agent.getLocation()
                if movetarget == None or selfposition == None:
                    return None
                ori = math.atan((selfposition[1] - movetarget[1]) / (movetarget[0] - selfposition[0]))/3.14*180
                if movetarget[0] < selfposition[0]:
                    ori = ori - 180
                if ori < 0:
                    ori = ori + 360
                self.agent.dodge(ori)

            # executing
            return None
        return ret

class MyMidRetreat(BTNode):
    ### percentage: Percentage of hitpoints that must have been lost

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.percentage = 0.5
        # First argument is the factor
        if len(args) > 0:
            self.percentage = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.id = args[1]

    def enter(self):
        BTNode.enter(self)
        self.agent.navigateTo(self.agent.world.getBaseForTeam(self.agent.getTeam()).getLocation())

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() > self.agent.getMaxHitpoints() * self.percentage:
            # fail executability conditions
            print "exec", self.id, "false"
            return True
        elif self.agent.getHitpoints() == self.agent.getMaxHitpoints():
            # Exection succeeds
            print "exec", self.id, "true"
            return True
        else:
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
                if distance(e.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
                    self.agent.turnToFace(e.getLocation())
                    self.agent.shoot()
                    break

            lines = self.agent.world.getLines()
            faster = True
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    faster = False
                    break
            if faster:
                movetarget = self.agent.moveTarget
                selfposition = self.agent.getLocation()
                if movetarget == None or selfposition == None:
                    return None
                ori = math.atan((selfposition[1] - movetarget[1]) / (movetarget[0] - selfposition[0]))/3.14*180
                if movetarget[0] < selfposition[0]:
                    ori = ori - 180
                if ori < 0:
                    ori = ori + 360
                self.agent.dodge(ori)

            # executing
            return None
        return ret



class MyRunAway(BTNode):
    ### target: a point (x, y)

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.percentage = 0.5
        # First argument is the factor
        if len(args) > 0:
            self.target = args[0]
        if len(args) > 1:
            self.percentage = args[1]
        # Second argument is the node ID
        if len(args) > 2:
            self.id = args[2]

    def enter(self):
        BTNode.enter(self)
        self.agent.navigateTo(self.target)

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() > self.agent.getMaxHitpoints() * self.percentage:
            # fail executability conditions
            print "exec", self.id, "false"
            return False
        if self.target == None:
            # failed executability conditions
            print "exec", self.id, "false"
            return False
        elif distance(self.agent.getLocation(), self.target) < self.agent.getRadius():
            # Execution succeeds
            print "exec", self.id, "true"
            return True
        else:
            # executing
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
                if distance(e.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
                    self.agent.turnToFace(e.getLocation())
                    self.agent.shoot()
                    break
            lines = self.agent.world.getLines()
            faster = True
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    faster = False
                    break
            if faster:
                movetarget = self.agent.moveTarget
                selfposition = self.agent.getLocation()
                if movetarget == None or selfposition == None:
                    return None
                ori = math.atan((selfposition[1] - movetarget[1]) / (movetarget[0] - selfposition[0])) / 3.14 * 180
                if movetarget[0] < selfposition[0]:
                    ori = ori - 180
                if ori < 0:
                    ori = ori + 360
                self.agent.dodge(ori)
            return None
        return ret


class MyMoveToTarget(BTNode):
    ### target: a point (x, y)

    def parseArgs(self, args):
        BTNode.parseArgs(self, args)
        self.target = None
        # First argument is the target
        if len(args) > 0:
            self.target = args[0]
        # Second argument is the node ID
        if len(args) > 1:
            self.percentage = args[1]
        if len(args) > 2:
            self.id = args[2]

    def enter(self):
        BTNode.enter(self)
        self.agent.navigateTo(self.target)

    def execute(self, delta=0):
        ret = BTNode.execute(self, delta)
        if self.agent.getHitpoints() <= self.percentage * self.agent.getMaxHitpoints():
            print "low hitpoint"
            return False
        if self.target == None:
            # failed executability conditions
            print "exec", self.id, "false"
            return False
        elif distance(self.agent.getLocation(), self.target) < self.agent.getRadius():
            # Execution succeeds
            print "exec", self.id, "true"
            return True
        else:
            # executing
            enemies = self.agent.world.getEnemyNPCs(self.agent.getTeam())
            for e in enemies:
                if distance(e.getLocation(), self.agent.getLocation()) <= self.agent.getRadius()*AREAEFFECTRANGE + e.getRadius():
                    self.agent.areaEffect()
                if distance(e.getLocation(), self.agent.getLocation()) <= BIGBULLETRANGE:
                    self.agent.turnToFace(e.getLocation())
                    self.agent.shoot()
                    break
            lines = self.agent.world.getLines()
            faster = True
            for l in lines:
                if minimumDistance(l, self.agent.getLocation()) <= self.agent.getMaxRadius() * 2:
                    faster = False
                    break
            if faster:
                movetarget = self.agent.moveTarget
                selfposition = self.agent.getLocation()
                if movetarget == None or selfposition == None:
                    return None
                ori = math.atan((selfposition[1] - movetarget[1]) / (movetarget[0] - selfposition[0])) / 3.14 * 180
                if movetarget[0] < selfposition[0]:
                    ori = ori - 180
                if ori < 0:
                    ori = ori + 360
                self.agent.dodge(ori)
            return None
        return ret